#ifndef __BIRD_H__
#define __BIRD_H__

#include "cocos2d.h"

class Bird
{
    
    
public:
    Bird(cocos2d::Node * node);
    void Fall();
    void Fly()
    {
        isFalling = false;
    };
    void StopFlying()
    {
        isFalling = true;
    }
  
    
    
private:
    cocos2d::Size visibleSize;
    cocos2d::Vec2 origin;
    bool isFalling;
    cocos2d::Sprite * flappyBird;
    
};

#endif
