#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"
#include "Pipe.h"
#include "Bird.h"


class GameScene : public cocos2d::Node
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(GameScene);
    
private:
    
    void setPhysicWord(cocos2d::PhysicsWorld *world){
        sceneWorld = world;
    }
    
    void SpawnPipe( float dt );
    
    bool onContactBegin( cocos2d::PhysicsContact& contact );
    bool onTouchBegan ( cocos2d::Touch *touch, cocos2d::Event *event);
    void StopFlying( float dt );
    void Update( float dt );
    
    cocos2d::PhysicsWorld *sceneWorld;
    Pipe pipe;
    Bird * bird;
};

#endif // __HELLOWORLD_SCENE_H__
