#include "Pipe.h"
#include "Definitions.h"

USING_NS_CC;

Pipe::Pipe(){
    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();
}

void Pipe::SpawnPipe(cocos2d::Node *node){

    CCLOG("SPAWN PIPE");
    auto topPipe = Sprite::create("Pipe.png");
    auto bottomPipe = Sprite::create("Pipe.png");
    
    auto topPipeBody = PhysicsBody::createBox(topPipe->getContentSize());
    auto bottomPipeBody = PhysicsBody::createBox(bottomPipe->getContentSize());
    
    auto random = CCRANDOM_0_1();
    
    if (random<LOWER_SCREEN_PIPE_THRESH_HOLD) {
        random = LOWER_SCREEN_PIPE_THRESH_HOLD;
    }
    else if(random>UPPER_SCREEN_PIPE_THRESH_HOLD){
        random = UPPER_SCREEN_PIPE_THRESH_HOLD;
    }
    
    auto topPipePosition = (random*visibleSize.height) + (topPipe->getContentSize().height/2);
    
    //what is setDynamic use for
    topPipeBody->setDynamic(false);
    bottomPipeBody->setDynamic(false);
    
    topPipeBody->setCollisionBitmask(OBSTACLE_COLLISION_BITMASK);
    bottomPipeBody->setCollisionBitmask(OBSTACLE_COLLISION_BITMASK);
    topPipeBody->setContactTestBitmask(true);
    bottomPipeBody->setContactTestBitmask(true);
    
    
    topPipe->setPhysicsBody(topPipeBody);
    bottomPipe->setPhysicsBody(bottomPipeBody);
    
    topPipe->setPosition(Point(visibleSize.width + topPipe -> getContentSize().width+origin.x, topPipePosition));
    
    bottomPipe->setPosition(Point(topPipe->getPositionX(), topPipePosition - (Sprite::create("Ball.png")->getContentSize().height*PIPE_GAP)-topPipe->getContentSize().height));
                                      
    node->addChild(topPipe);
    node->addChild(bottomPipe);
    
    auto topPipeAction = MoveBy::create(PIPE_MOVEMENT_SPEED * visibleSize.width, Point(-visibleSize.width*1.5, 0));
    auto bottomPipeAction = MoveBy::create(PIPE_MOVEMENT_SPEED * visibleSize.width, Point(-visibleSize.width*1.5, 0));
    
    topPipe->runAction(topPipeAction);
    bottomPipe->runAction(bottomPipeAction);
}